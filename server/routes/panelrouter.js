const express = require('express')
const router = new express.Router()
const authMiddleware = require('../authserver/middlewares/authMW')
const ArticleController = require('../controllers/posts/ArticleController')
const NewsController = require('../controllers/posts/NewsController')
const IdeologyController = require('../controllers/posts/IdeologyController')
const { UnathorizedError } = require('../authserver/errors/Errors')

// article routes

router.post('/article/create', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    ArticleController.create(req, res, next)
  } catch (error) {
    next(error)
  }
})

router.post('/article/update', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    ArticleController.update(req, res, next)
  } catch (error) {
    next(error)
  }
})

router.post('/article/delete', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    ArticleController.delete(req, res, next)
  } catch (error) {
    next(error)
  }
})

// news routes

router.post('/news/create', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    NewsController.create(req, res, next)
  } catch (error) {
    next(error)
  }
})

router.post('/news/update', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    NewsController.update(req, res, next)
  } catch (error) {
    next(error)
  }
})

router.post('/news/delete', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    NewsController.delete(req, res, next)
  } catch (error) {
    next(error)
  }
})

// ideology routes

router.post('/ideology/create', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    IdeologyController.create(req, res, next)
  } catch (error) {
    next(error)
  }
})

router.post('/ideology/update', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    IdeologyController.update(req, res, next)
  } catch (error) {
    next(error)
  }
})

router.post('/ideology/delete', authMiddleware, async (req, res, next) => {
  try {
    if (!req.isLogged) next(new UnathorizedError('Not logged in'))
    IdeologyController.delete(req, res, next)
  } catch (error) {
    next(error)
  }
})

module.exports = router