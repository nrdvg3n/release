const express = require('express')
const router = new express.Router()
const Article = require('../models/Article')
const News = require('../models/News')
const Ideology = require('../models/Ideology')

router.get('/article', async (req, res, next) => {
  try {
    const articles = await Article.find()
    res.json({data: articles})
  } catch (error) {
    next(error)
  }
})

router.get('/news', async (req, res, next) => {
  try {
    const news = await News.find()
    res.json({data: news})
    
  } catch (error) {
    next(error)
  }
})

router.get('/ideology', async (req, res, next) => {
  try {
    const ideology = await Ideology.find()
    res.json({data: ideology})
    
  } catch (error) {
    next(error)
  }
})

module.exports = router