const mongoose = require('mongoose')

const articleSchema = mongoose.Schema({
  title: {
    type: String, 
    required: true
  },
  content: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  tags: Array,
  author: String
})

const Article = mongoose.model('Article', articleSchema)

module.exports = Article