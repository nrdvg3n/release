const mongoose = require('mongoose')

const NewsSchema = mongoose.Schema({
  title: {
    type: String, 
    required: true
  },
  content: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  author: String
})

const News = mongoose.model('News', NewsSchema)

module.exports = News