const mongoose = require('mongoose')

const IdeologySchema = mongoose.Schema({
  title: {
    type: String, 
    required: true
  },
  content: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  tags: Array,
  author: String
})

const Ideology = mongoose.model('Ideology', IdeologySchema)

module.exports = Ideology