const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const config = require('./config')
const mongoose = require('mongoose')
const homerouter = require('./routes/homerouter')
const panelrouter = require('./routes/panelrouter')
const authRouter = require('./authserver/routes/authRouter')
const authIndexRouter = require('./authserver/routes/indexRouter')

const app = express()

//CORS-politics
app.use(cors(config.cors))

//Parsing
app.use(express.json())
app.use(cookieParser('secretkey-4281828741'))

//Logging
app.use('', (req, res, next) => {
  console.log(req.method, req.url, req.body)
  next()
})


//Routing
app.use('/auth', authRouter)
app.use('/auth', authIndexRouter)
app.use('/home', homerouter)
app.use('/panel', panelrouter)

//ErrorHandler
app.use((err, req, res, next) => {
  console.log(err)
  res.json({data: err, error: true})
})

//Trycatch
const start = () => {
  try {
    mongoose.connect(config.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true }) //Connect database
    app.listen(PORT = config.PORT, () => {
      console.log('Server is running on port', PORT)
    })
  } catch (error) {
    console.log(error)
  }
}

start()