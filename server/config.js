module.exports = {
  PORT: process.env.PORT || 8080,
  mongoURI: process.env.MONGOURI || 'mongodb+srv://user:6HnhZG5eseJKVJc@cluster0.841ly.mongodb.net/db1?retryWrites=true&w=majority',
  jwt: {
    accessSecret: process.env.ACCESS || "mysecret-lk3k5jhh643bhg32vhgr3",
    refreshSecret: process.env.REFRESH || "mysecret-wifmsc39zpqksc93nfds4",
  },
  cors: {
    origin: process.env.ORIGIN || "http://localhost:3000",
    optionsSuccessStatus: 200,
    allowedHeaders: ['Content-Type'],
    credentials: true
  }
}