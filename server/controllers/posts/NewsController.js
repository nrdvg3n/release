const News = require('../../models/News')

class NewsController {
  async create (req, res, next) {
    try {
      const post = req.body.data
      post.author = req.userinfo.username
      post._id = undefined
      const newPost = new News(post)
      await newPost.save()
      res.json({data: 'Success'})

    } catch (error) {
      next(error)
    }
  }

  async update (req, res, next) {
    try {
      const post = req.body.data
      if (!post._id) next('Invalid data')
      await News.findByIdAndUpdate(post._id, post)
      res.json({data: 'Success'})
      
    } catch (error) {
      next(error)
    }
  }

  async delete (req, res, next) {
    try {
      const post = req.body.data
      if (!post._id) next('Invalid data')
      await News.findByIdAndDelete(post._id)
      res.json({data: 'Success'})
    } catch (error) {
      next(error)
    }
  }
}

module.exports = new NewsController()