const Article = require('../../models/Article')

class ArticleController {
  async create (req, res, next) {
    try {
      const post = req.body.data
      post.author = req.userinfo.username
      post._id = undefined
      const newPost = new Article(post)
      await newPost.save()
      res.json({data: 'Success'})

    } catch (error) {
      next(error)
    }
  }

  async update (req, res, next) {
    try {
      const post = req.body.data
      if (!post._id) next('Invalid data')
      await Article.findByIdAndUpdate(post._id, post)
      res.json({data: 'Success'})
      
    } catch (error) {
      next(error)
    }
  }

  async delete (req, res, next) {
    try {
      const post = req.body.data
      if (!post._id) next('Invalid data')
      await Article.findByIdAndDelete(post._id)
      res.json({data: 'Success'})
    } catch (error) {
      next(error)
    }
  }
}

module.exports = new ArticleController()